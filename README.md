# Projekt Genetyka - Program do analizy cech genetycznych

Ten projekt jest programem napisanym w języku C++, który ma na celu analizowanie cech genetycznych i określanie podobieństwa między rodzicami a dzieckiem. Program został stworzony jako część projektu z przedmiotu POP (Podstawy Programowania) w 2018 roku na Politechnice Gdańskiej.

## Instrukcje

1. Uruchomienie programu
   - Skompiluj program przy użyciu środowiska CodeBlocks w wersji 17.12 i kompilatora GNU GCC.
   - Uruchom skompilowany program.

2. Podawanie cech genetycznych
   - Podaj cechy genetyczne mężczyzny (ojca) w formacie: "Czyje_cechy: cecha1, cecha2, cecha3, cecha4". Na przykład: "Mężczyzna: oczy czerwone, włosy blond, leworęczność".
   - Następnie podaj cechy genetyczne kobiety (matki) w odpowiedniej kolejności. Na przykład: "Kobieta: oczy zielone, ciemne włosy, praworęczność".
   - Pamiętaj, aby używać dwukropka po słowie "Mężczyzna" i oddzielać poszczególne cechy przecinkami.
   - Cechy podawaj małymi literami, np. "brązowe oczy", a nie "BRĄZOWE OCZY".

3. Analiza cech dziecka
   - Następnie podaj cechy genetyczne dziecka w formacie: "Płeć: cecha1, cecha2, cecha3". Na przykład: "Dziewczynka: oczy niebieskie, blond włosy, leworęczność" lub "Chłopiec: oczy niebieskie, ciemne włosy, praworęczność".
   - Pamiętaj, aby określić płeć dziecka, dodając na początku "Dziewczynka:" lub "Chłopiec:".

4. Wynik analizy
   - Program analizuje podane cechy rodziców i dziecka.
   - Na ekranie zostaje wypisana tabela porównująca cechy i ich zgodność.
   - Jeśli cechy dziecka są zgodne z cechami rodziców, zostanie wyświetlony komunikat: "Cechy rodziców i dziecka są zgodne".
   - Jeśli cechy dziecka nie są zgodne z cechami rodziców, zostanie wyświetlony komunikat: "Dziecko jest adoptowane".

## Uwagi dotyczące uruchomienia programu

- Projekt został napisany w środowisku CodeBlocks w wersji 17.12 przy użyciu kompilatora GNU GCC. Upewnij się, że masz te narzędzia zainstalowane i skonfigurowane na swoim komputerze przed uruchomieniem programu.
- Projekt używa przestrzeni nazw "std" oraz biblioteki "iostream" i "string". Upewnij się, że masz te biblioteki dostępne w swoim środowisku programistycznym.
- Przed uruchomieniem programu skompiluj go za pomocą dostępnego w środowisku CodeBlocks kompilatora GNU GCC.
- Po uruchomieniu programu postępuj zgodnie z instrukcjami wyświetlanymi na ekranie.

**Uwaga:** Ten kod jest starszym projektem, a jego czytelność i struktura mogą być nieco ograniczone. Jeśli masz pytania dotyczące funkcjonowania programu lub potrzebujesz dalszej pomocy, chętnie ci w tym pomogę.
