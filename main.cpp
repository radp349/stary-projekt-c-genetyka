// POP 2018-12-12 projekt 1 Pawelec Radosław EIT 2 175631       CodeBlocks 17.12  GNU GCC COMPILER
#include <iostream>
#include <string>

using namespace std;



   string dominacja (string cecha)  //Funkcja określająca czy cecha jest dominująca
{string

CechyDominujace[50]={"oczy piwne","oczy ciemne","oczy br\245zowe","prawor\251czno\230\206","brak pieg\242w","d\210ugie rz\251sy","rz\251sy d\210ugie",
"w\210osy kr\251cone","w\210osy ciemne","w\210osy br\245zowe","rh+","krew rh+","grupa krwi rh+","wyst\251powanie grupy rh","brak daltonizmu","piwne oczy",
"ciemne oczy","br\245zowe oczy","kr\251cone w\210osy",
"ciemne w\210osy","br\245zowe w\210osy","wyst\251powanie grupy krwi rh","w\210osy czarne","czarne w\210osy","ow\210osienie","obfite ow\210osienie","ow\210osienie cia\210a",
"obfite ow\210osienie cia\210a","\210ysienie","sk\210onno\230\206 do \210ysienia","szerokie brwi","rozdzielone brwi","oddzielne brwi",
"garbaty nos","krzywy nos","odstaj\245ce uszy","orzechowe oczy","oczy orzechowe","astygmatyzm","kr\242tkowzroczno\230\206","dalekowzroczno\230\206","du\276e oczy","niski wzrost",
"szerokie wargi","owalna twarz","nerwowo\230\206","nieumiej\251tno\230\206 zwijania j\251zyka w tr\245bk\251","brak umiej\251tno\230ci zwijania j\251zyka w tr\245bk\251",
"nieumiej\251tno\230\206 zwijania j\251zyka","brak umiej\251tno\230ci zwijania j\251zyka"};
int d=0;

for(int i=0;i<50;i++)
{
    if(cecha==CechyDominujace[i]){return "1";}  //jeśli cecha z maina jest identyczna z cechą w powyższej bazie, to mówimy mainowi, że jest to cecha dominująca
}

return "0"; //jeśli cecha nie jest dominująca, to zwracamy 0
}


   string przeciwna (string cecha) //dla zadanej cechy dominującej, zwracamy cechę do niej recesywną
{
    string CechyDominujace[50] /*baza cech dominujących*/={"oczy piwne","oczy ciemne","oczy br\245zowe","prawor\251czno\230\206","brak pieg\242w","kr\242tkie rz\251sy","rz\251sy kr\242tkie",
"w\210osy kr\251cone","w\210osy ciemne","w\210osy br\245zowe","rh+","krew rh+","grupa krwi rh+","wyst\251powanie grupy rh","brak daltonizmu","piwne oczy",
"ciemne oczy","br\245zowe oczy","kr\251cone w\210osy",
"ciemne w\210osy","br\245zowe w\210osy","wyst\251powanie grupy krwi rh","w\210osy czarne","czarne w\210osy","ow\210osienie","obfite ow\210osienie","ow\210osienie cia\210a",
"obfite ow\210osienie cia\210a","\210ysienie","sk\210onno\230\206 do \210ysienia","szerokie brwi","rozdzielone brwi","oddzielne brwi",
"garbaty nos","krzywy nos","odstaj\245ce uszy","orzechowe oczy","oczy orzechowe","astygmatyzm","kr\242tkowzroczno\230\206","dalekowzroczno\230\206","du\276e oczy","niski wzrost",
"szerokie wargi","owalna twarz","nerwowo\230\206","nieumiej\251tno\230\206 zwijania j\251zyka w tr\245bk\251","brak umiej\251tno\230ci zwijania j\251zyka w tr\245bk\251",
"nieumiej\251tno\230\206 zwijania j\251zyka","brak umiej\251tno\230ci zwijania j\251zyka"},

CechyRecesywne[50] /*baza cech recesywnych*/={"oczy niebieskie","oczy jasne","oczy niebieskie","lewor\251czno\230\206","piegi","d\210ugie rz\251sy","rz\251sy d\210ugie","w\210osy proste","w\210osy blond",
"w\210osy blond","rh-","krew rh-","grupa krwi rh-","brak wyst\251powania grupy rh","daltonizm","niebieskie oczy",
"jasne oczy","niebieskie oczy","proste w\210osy",
"blond w\210osy","blond w\210osy","brak wyst\251powania grupy krwi rh","w\210osy blond","blond w\210osy","brak ow\210osienia","ma\210o obfite ow\210osienie","brak ow\210osienia cia\210a",
"brak obfitego ow\210osienia cia\210a","brak \210ysienia","brak sk\210onno\230ci do \210ysienia","w\245skie brwi","z\210\245czone brwi","z\210\245czone brwi",
"prosty nos","prosty nos","uszy przylegaj\245ce do cia\210a","niebieskie oczy","oczy niebieskie","brak astygmatyzmu","brak kr\242tkowzroczno\230ci","brak dalekowzroczno\230ci","ma\210e oczy",
"normalny wzrost","w\245skie wargi","kwadratowa twarz","spokojny temperament","umiej\251tno\230\206 zwijania j\251zyka w tr\245bk\251","umiej\251tno\230\206 zwijania j\251zyka w tr\245bk\251",
"umiej\251tno\230\206 zwijania j\251zyka"," umiej\251tno\230\206 zwijania j\251zyka"};
int d=0;

for(int i=0;i<50;i++)
{
    if(cecha==CechyDominujace[i]){return CechyRecesywne[i];} //jeśli istnieje cecha recesywna do zadanej cechy dominującej, to recesywną zwracamy
}

return "0";
}


void wypisz_instrukcje_1()
{
cout<<"Podaj cechy genetyczne M\251\276czyny(Ojca), w nast\251puj\245cy spos\242b: "<<endl;
cout<<"Czyje_cechy: cecha1,cecha2, cecha3, cecha4         Przyk\210adowo: "<<endl;
cout<<"M\251\276czyzna: oczy czerwone, w\210osy blond, lewor\251czno\230\206 "<<endl;
cout<<"Pami\251taj o dwukropku po s\210owie: M\251\276czyna i oddzielaniu poszczeg\242lnych cech przecinkiem! "<<endl;
cout<<"Cechy nale\276y podawa\206 w spos\242b bezosobowy, np prawor\251czno\230\206 zamiast prawor\251czny; piegi zamiast piegowaty "<<endl;
cout<<"Pisz ma\210ymi literami  np  br\245zowe oczy, zamiast BR\244ZOWE OCZY "<<endl<<endl;
}

void wypisz_instrukcje_2()
{
cout<<"Teraz podaj odpowiednie cechy genetyczne Kobiety(Matki), w adekwatnej kolejno\230ci, np. "<<endl;
cout<<"Je\206li wpisa\210e\230 M\251\276czyzna: oczy niebieskie, w\210osy blond, lewor\251czno\230\206      To adekwatne b\251dzie"<<endl;
cout<<"               Kobieta: oczy zielone, ciemne w\210osy, prawor\251czno\230\206 "<<endl;
cout<<"Pami\251taj o dwukropku po s\210owie: Kobieta, a tak\276e o oddzielaniu poszczeg\242lnych cech przecinkiem! "<<endl;
cout<<"Cechy nale\276y podawa\206 w spos\242b bezosobowy, np prawor\251czno\230\206 zamiast prawor\251czna; piegi zamiast piegowata "<<endl;
cout<<"Pisz ma\210ymi literami  np  br\245zowe oczy, zamiast BR\244ZOWE OCZY "<<endl<<endl;
}

void pisz_linie()
{cout<<"_________________________________________________________________________________________________________"<<endl;
}


void na_male_ltery(string &s)
{
        for(int i = 0; i < s.length(); i++)
        {
          s[i] = tolower(s[i]);
        }}







int main()
{
int ile_cechM=1, ile_cechK=1,ile_cechD=1, //zmienne przechowujące ilość podanych cech genetycznych (mężczyzny, kobiety, dziecka)
p=0,r=0, //zmienne pomagające w przejściu z początkowego opisu semantycznego, na tablicę cech genetycznych

dg,g,h,  //zmienne które służa do estetycznego wypisania tabeli na ekran

podobienstwa,zgodnosc; //zmienne do testu na ojcostwo

    string CechyM,CechyK,CechyD, //do tych stringów zapisujemy podane przez użytkownika (w opisie semantycznym) cechy genetyczne
    szukaj1=":",szukaj2=",",spacja=" ",zero="0",jeden="1",chlopiec="ch\210opiec",dziewczynka="dziewczynka",X="X",Y="Y",  //przydatne słowa i znaki
    czy_dominujaca  //zmienna wysyłana do funkcji Dominacja

    , lewa, prawa, //zmienne pomagające w estetycznym wypisaniu tabeli na ekran
    przeciwnosc; //zmienna przechowująca rezultat wywołania funkcji Przeciwna


wypisz_instrukcje_1();

getline(cin, CechyM);  //pobieramy cechy mężczyzny

na_male_ltery(CechyM); //pozbywamy się dużych liter

size_t pozycjaM=CechyM.find(szukaj1); //ustalamy pozycje dwukropka

p=pozycjaM+1; //przechodzimy na pozycję pierwszego znaku, pierwszej cechy (zaraz po dwukropku)

for(int i=0;i<CechyM.length();i++)
{
    if(CechyM[i]==szukaj2[0]) {ile_cechM++;}  //liczymy ile cech podano  (liczba przecinków + 1)
}
CechyM+=szukaj2[0];  //dodajemy na końcu przecinek, który pozwoli pozbyć się zbędnych spacji


string Mcechy[ile_cechM]; //w tej tablicy zapiszemy tymczasowo, podane cecy mężczyzny


while(p<CechyM.length())
{
    if(((int)CechyM[p]==32)&&((CechyM[p-1]==szukaj2[0])||(CechyM[p+1]==szukaj2[0])||(CechyM[p-1]==szukaj1[0]))){} //jeśli spacja znajduje się koło przecinka lub dwukropka, to jej nie zapisujemy

    else if(CechyM[p]!=szukaj2[0]){Mcechy[r]+=CechyM[p];} //jeśli znak nie jest orzecinkiem, ani zbędną spacją, to go zapisujemy

    else {r++;} //trafiliśmy na przecinek, więc przechodzimy do kolejnej cechy

    p++; //przechodzimy na następny znak
}
r=0;  //zerujemy zmienną, bo będzie jeszcze potrzebn dla kobiety i dziecka



cout<<endl;

// ROBIMY TO SAMO CO PRZED CHWILĄ, ALE TYM RAZEM DLA CECH KOBIETY
wypisz_instrukcje_2();

getline(cin, CechyK);

na_male_ltery(CechyK);

size_t pozycjaK=CechyK.find(szukaj1);

p=pozycjaK+1;


for(int i=0;i<CechyK.length();i++)
{
if(CechyK[i]==szukaj2[0]) {ile_cechK++;}
}

CechyK+=szukaj2[0];

string Kcechy[ile_cechK];

while(p<CechyK.length())
{

if(((int)CechyK[p]==32)&&((CechyK[p-1]==szukaj2[0])||(CechyK[p+1]==szukaj2[0])||(CechyK[p-1]==szukaj1[0]))){}

else if(CechyK[p]!=szukaj2[0]){Kcechy[r]+=CechyK[p];}

else {r++;}

p++;

}

r=1; //dziecko posiada dodatkową cechę w postaci płci, dlatego r=0, a nie 1

// ROBIMY TO SAMO CO PRZED CHWILĄ, ALE TYM RAZEM DLA CECH KOBIETY



string Tabela[5][9][ile_cechK+1][2];  //Tworzymy tabelę, którą będziemy wypisywać na ekran
//Pierwsze 2 wymiary opisuą pozycje w tabeli
//Trzeci opisuje linijkę tekstu (wypisujemy jak drukarka
//Czwarty strwierdza czy cecha jest dominująca



for (int i=0;i<ile_cechK+1;i++)
{Tabela[0][0][i][0]="                    ";}  //tworzymy prawy górny róg tabeli



for(int x=1;x<5;x++)
{

    for(int z=1;z<=ile_cechK+1;z++)

                {if (z>1) {czy_dominujaca=Kcechy[z-2];
                przeciwnosc=przeciwna(czy_dominujaca); //szukamy cechy przeciwnej (recesywnej), dla danej cechy dominującej
                Tabela[x][0][z-1][1]=dominacja(czy_dominujaca);} //sprawdzamy czy nasza cecha jest dominująca

        if(z==1){Tabela[x][0][z-1][0]+=X[0];}  //w pierwsze linijce wpisujemy wszędzie X


        else if((Tabela[x][0][z-1][1]==jeden)&&(x==1)&&(przeciwnosc!=zero)){Tabela[x][0][z-1][0]=przeciwnosc; Tabela[x][0][z-1][1]=zero;}
                                                                                            /*uwzględniamy że osoba posiadająca cechę dominującą, może być nosicielem cechy recesywnej*/

        else if((Tabela[x][0][z-1][1]==jeden)&&(x==1)){Tabela[x][0][z-1][0]=Mcechy[z-2];}
                                                                           /*uwzględniamy że osoba posiadająca cechę dominującą, może być nosicielem cechy recesywnej, której nie ma w bazie*/


        else if(Tabela[x][0][z-1][1]==zero) {Tabela[x][0][z-1][0]=Kcechy[z-2];} //cechy recesywne spisujemy jak leci



        else if((Tabela[x][0][z-1][1]==jeden)&&(x>1)){Tabela[x][0][z-1][0]=Kcechy[z-2];} //spisujemy cechy dominujące
    }
}



for(int y=1;y<5;y++) //pętla analogiczna do powyższej
{
    for(int z=0;z<ile_cechK+1;z++)

                {if (z>0) {czy_dominujaca=Mcechy[z-1];
                przeciwnosc=przeciwna(czy_dominujaca);
                Tabela[0][y][z][1]=dominacja(czy_dominujaca);}

        if(z==0){Tabela[0][y][z][0]+=X[0];}

        else if((Tabela[0][y][z][1]==jeden)&&(y==1)&&(przeciwnosc!=zero)){Tabela[0][y][z][0]=przeciwnosc; Tabela[0][y][z][1]=zero;}

        else if((Tabela[0][y][z][1]==jeden)&&(y==1)){Tabela[0][y][z][0]=Kcechy[z-1];}

        else if(Tabela[0][y][z][1]==zero) {Tabela[0][y][z][0]=Mcechy[z-1];}

        else if((Tabela[0][y][z][1]==jeden)&&(y>1)) {Tabela[0][y][z][0]=Mcechy[z-1];}
    }
}




for(int y=5;y<9;y++) //pętla analogiczna do powyższej
{
    for(int z=0;z<ile_cechK+1;z++)
                {if (z>0) {czy_dominujaca=Mcechy[z-1];
                przeciwnosc=przeciwna(czy_dominujaca);
                Tabela[0][y][z][1]=dominacja(czy_dominujaca);}

        if(z==0){Tabela[0][y][z][0]+=Y[0];}

        else if((Tabela[0][y][z][1]==jeden)&&(y==5)&&(przeciwnosc!=zero)){Tabela[0][y][z][0]=przeciwnosc; Tabela[0][y][z][1]=zero;}

        else if((Tabela[0][y][z][1]==jeden)&&(y==5)){Tabela[0][y][z][0]=Kcechy[z-1];}

        else if(Tabela[0][y][z][1]==zero) {Tabela[0][y][z][0]=Mcechy[z-1];}

        else if((Tabela[0][y][z][1]==jeden)&&(y>5)){Tabela[0][y][z][0]=Mcechy[z-1];}
    }
}




for(int x=1;x<5;x++)  //pętla która wpisuje do tabeli, możliwe kombinacje cech dziecka
{
    for(int y=1;y<9;y++)
    {
        for(int z=0;z<ile_cechK+1;z++)
        {
            if((z==0)&&(Tabela[0][y][0][0]==X)){Tabela[x][y][z][0]+=dziewczynka;}  //XX= dziewczynka

            else if((z==0)&&(Tabela[0][y][0][0]==Y)){Tabela[x][y][z][0]+=chlopiec;} //XY- chłopiec

            else if (Tabela[x][0][z][0]==Tabela[0][y][z][0]){Tabela[x][y][z][0]+=Tabela[x][0][z][0];}  //sprawdzamy czy ojciec i matka, mają identyczną cechę

            else if ((Tabela[x][0][z][1]==jeden)&&(Tabela[0][y][z][1]==jeden)&&(x%2==0)) {Tabela[x][y][z][0]+=Tabela[x][0][z][0];}
                                                                                        /*Rozważamy przypadek, gdy rodzice mają 2 różne cechy dominujące np. czarne włosy i brązowe włosy*/
            else if ((Tabela[x][0][z][1]==jeden)&&(Tabela[0][y][z][1]==jeden)&&(x%2==1)) {Tabela[x][y][z][0]+=Tabela[0][y][z][0];}

            else if (Tabela[x][0][z][1]==jeden){Tabela[x][y][z][0]+=Tabela[x][0][z][0];}
                                                        //jeśli porównujemy cechę recesywną i dominującą, to przepisujemy dominującą
            else if (Tabela[0][y][z][1]==jeden){Tabela[x][y][z][0]+=Tabela[0][y][z][0];}

            else if ((Tabela[0][y][z][1]==zero)&&(x%2==0)){Tabela[x][y][z][0]+=Tabela[0][y][z][0];}
                                                    //jeśli oboje rodziców ma cechy recesywne, to spisujemy je na krzyż
            else if ((Tabela[x][0][z][1]==zero)&&(x%2==1)){Tabela[x][y][z][0]+=Tabela[x][0][z][0];}
        }

    }

}



cout<<endl<<endl;




for(int y=0;y<9;y++) //wypisujemy tabelę na ekran
{
    for(int z=0;z<ile_cechK+1;z++)
    {
        for(int x=0;x<5;x++)
            {dg=Tabela[x][y][z][0].length();

            if(dg%2==0)    {h=g=(  (20-dg) /2  );}
                            /*obliczamy konieczną ilość spacji, aby tabela została wypisana w sposób estetyczny*/
            else    {h=(  (21-dg)  /2   );      g=((19-dg)/2);}


                for(int m=0;m<h;m++)
                    {lewa+=spacja ;}


                for(int n=0;n<g;n++)
                    {prawa+=spacja ;}


            cout<<lewa<<Tabela[x][y][z][0]<<prawa<<"|"; //wypisujemy komórkę

            lewa.clear();

            prawa.clear();
        }

    cout<<endl;

    }

pisz_linie(); //wypisujemy nowy wiersz
}




    cout<<endl<<endl;
    cout<<"Podaj cechy dziecka, aby sprawdzi\206 ich zgodno\230\206 z podanymi cechami rodzic\242w "<<endl;
    cout<<"Np. ch\210opiec: oczy niebieskie, blond w\210osy, lewor\251czno\230\206 "<<endl;
    cout<<"Lub np. dziewczynka: oczy niebieskie, ciemne w\210osy, prawor\251czno\230\206 "<<endl;
    cout<<" Wa\276ne jest aby okre\230li\206 p\210e\206   "<<endl;


    getline(cin, CechyD); //wczytujemy cechy dziecka, w ten sam sposób, co cechy mężczyny i kobiety na początku programu

    na_male_ltery(CechyD);

    size_t pozycjaD=CechyD.find(szukaj1);

    p=pozycjaD+1;



    for(int i=0;i<CechyD.length();i++)
    {
        if(CechyD[i]==szukaj2[0]) {ile_cechD++;}
    }
    CechyD+=szukaj2[0];

    string Dcechy[ile_cechD+1];

    while(p<CechyD.length())
    {
        if(((int)CechyD[p]==32)&&((CechyD[p-1]==szukaj2[0])||(CechyD[p+1]==szukaj2[0])||(CechyD[p-1]==szukaj1[0]))){}

        else if(CechyD[p]!=szukaj2[0]){Dcechy[r]+=CechyD[p];}

        else {r++;}

        p++;
    }



    if (CechyD[0]=='d'){Dcechy[0]=dziewczynka;}
                //sprawdzamy płeć dziecka, w oparciu o pierwszy znak
    else {Dcechy[0]=chlopiec;}



    cout<<endl<<endl;
    cout<<"Cechy m\251\276czyny to: "<<endl; //wypisujemy cechy mężczyzny
    for(int i=0;i<ile_cechM;i++)
    {
        cout<<Mcechy[i]<<endl;

    }




    cout<<endl;
    cout<<"Cechy kobiety to: "<<endl; //wypisujey cechy kobiety
    for(int i=0;i<ile_cechK;i++)
    {
        cout<<Kcechy[i]<<endl;

    }



    cout<<endl;
    cout<<"Cechy dziecka to: "<<endl; //wypisujemy cechy dziecka
    for(int i=0;i<ile_cechD+1;i++)
    {
        cout<<Dcechy[i]<<endl;

    }




    cout<<endl;
    for(int x=1;x<5;x++)
        {
            for(int y=1;y<9;y++)
            {
                for(int z=0;z<ile_cechD+1;z++)
                {if(Dcechy[z]==Tabela[x][y][z][0]){podobienstwa++;}  //porównujemy cechy dziecka, ze wszystkimy możliwymi kombinacjami dla danych rodziców


                }
            if(podobienstwa==ile_cechD+1){zgodnosc++;}  //sprawdzamy czy cechy dziecka są zgodne
            podobienstwa=0;
            }
        }
    if(zgodnosc>0){cout<<"Cechy rodzic\242w i dziecka s\245 zgodne";}
    else {cout<<"Dziecko jest adoptowane";}

    return 0;
}

